# Use the official Docker image
FROM docker:latest

# Install any extra tools or dependencies if needed
# RUN apk add --no-cache <additional-packages>

# Set up the Docker entrypoint
ENTRYPOINT ["dockerd-entrypoint.sh"]

# Default command
CMD ["sh"]